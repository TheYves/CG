precision mediump float;

varying vec4 vFragmentColor;
varying vec2 vTextureCoord;
varying vec3 vNormals;
varying vec3 vPositionCameraSpace;

uniform sampler2D uSampler;
uniform vec3 uLightPosition;

void main() {
    float kd = 2.5;
    float ka = 0.5;

    vec3 ambientLight = vec3(1, 1, 1);
    vec3 color = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t)).rgb;
    vec3 normal = normalize(vNormals);
    vec3 lightDirection = normalize(uLightPosition - vPositionCameraSpace);
    float diffuseLight = max(dot(normal, lightDirection), 0.0);

    gl_FragColor = vec4((diffuseLight * kd + ambientLight * ka) * color * vec3(vFragmentColor), 1.0);
}