precision mediump float;

attribute vec4 aVertexPosition;
attribute vec4 aVertexColor;
attribute vec2 aVertexTextureCoord;
attribute vec3 aVertexNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;
uniform mat3 uNormalMatrix;

varying vec4 vFragmentColor;
varying vec2 vTextureCoord;
varying vec3 vNormals;
varying vec3 vPositionCameraSpace;

void main() {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;

    vFragmentColor = aVertexColor;
    vTextureCoord = aVertexTextureCoord;
    vNormals = uNormalMatrix * aVertexNormal;
    vPositionCameraSpace = vec3(uModelViewMatrix * aVertexPosition) / aVertexPosition.w;
}
