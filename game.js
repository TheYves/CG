window.onload = startup;

var canvas;
var gl;
var shaderProgram;
var buffers = {};
var matrices = {};
var attributes = {};
var colors = {
    red: [1, 0, 0, 1],
    green: [0, 1, 0, 1],
    blue: [0, 0, 1, 1],
    yellow: [1, 1, 0, 1],
    magenta: [1, 0, 1, 1],
    cyan: [0, 1, 1, 1]
};
var cubePoints = {
    A: [-1, 1, 1],
    B: [1, 1, 1],
    C: [1, -1, 1],
    D: [-1, -1, 1],

    E: [-1, 1, -1],
    F: [1, 1, -1],
    G: [1, -1, -1],
    H: [-1, -1, -1]
};
var cube = {
    rotation: 0,
    vertices: cubePoints.A.concat(
        cubePoints.B, cubePoints.C, cubePoints.D, // front
        cubePoints.B, cubePoints.F, cubePoints.G, cubePoints.C, // right
        cubePoints.F, cubePoints.E, cubePoints.H, cubePoints.G, // back
        cubePoints.E, cubePoints.A, cubePoints.D, cubePoints.H, // left
        cubePoints.E, cubePoints.F, cubePoints.B, cubePoints.A, // top
        cubePoints.D, cubePoints.C, cubePoints.G, cubePoints.H // bottom
    ),
    normals: [
        0, 0, 1,
        0, 0, 1,
        0, 0, 1,
        0, 0, 1,

        1, 0, 0,
        1, 0, 0,
        1, 0, 0,
        1, 0, 0,

        0, 0, -1,
        0, 0, -1,
        0, 0, -1,
        0, 0, -1,

        -1, 0, 0,
        -1, 0, 0,
        -1, 0, 0,
        -1, 0, 0,

        0, 1, 0,
        0, 1, 0,
        0, 1, 0,
        0, 1, 0,

        0, -1, 0,
        0, -1, 0,
        0, -1, 0,
        0, -1, 0
    ],
    colors: colors.red.concat(
        colors.red, colors.red, colors.red, // front
        colors.green, colors.green, colors.green, colors.green, // right
        colors.blue, colors.blue, colors.blue, colors.blue, // back
        colors.magenta, colors.magenta, colors.magenta, colors.magenta, // left
        colors.yellow, colors.yellow, colors.yellow, colors.yellow, // top
        colors.cyan, colors.cyan, colors.cyan, colors.cyan // bottom
    ),
    drawVertexIndices: [
        3, 2, 0, 2, 1, 0, // front
        7, 6, 4, 6, 5, 4, // right
        11, 10, 8, 10, 9, 8, // back
        15, 14, 12, 14, 13, 12, // left
        19, 18, 16, 18, 17, 16, // top
        23, 22, 20, 22, 21, 20 // bottom
    ],
    textureCoords: [
        // front
        0, 1,
        1, 1,
        1, 0,
        0, 0,

        // right
        0, 1,
        1, 1,
        1, 0,
        0, 0,

        // back
        0, 1,
        1, 1,
        1, 0,
        0, 0,

        // left
        0, 1,
        1, 1,
        1, 0,
        0, 0,

        // top
        0, 1,
        1, 1,
        1, 0,
        0, 0,

        // bottom
        0, 1,
        1, 1,
        1, 0,
        0, 0
    ],
    texture: null, // gets loaded later
    image: new Image()
};

function startup() {
    initTextures();
    initWebGL();
    drawAnimation();
}

function initWebGL() {
    canvas = document.getElementById("gameCanvas");
    gl = createGLContext(canvas);

    gl.clearColor(1, 1, 1, 1);
    gl.viewport(0, 0, canvas.width, canvas.height);
    shaderProgram = loadAndCompileShaders(gl, 'VertexShader.shader', 'FragmentShader.shader');

    gl.frontFace(gl.CCW);     // defines how the front face is drawn
    gl.cullFace(gl.BACK);     // defines which face should be culled
    gl.enable(gl.CULL_FACE);  // enables culling

    setupAttributes();
    setupBuffers();
    setupMatrices();
}

function initTextures() {
    cube.image.onload = function () {
        loadTexture(cube.image);
    };
    cube.image.src = "texture1.jpg";
}

function loadTexture(image) {
    // create a new texture object and make it the active texture
    cube.texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, cube.texture);

    // set parameters
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);

    // turn texture off again
    gl.bindTexture(gl.TEXTURE_2D, null);
}

function setupMatrices() {
    matrices.cube = mat4.create();
    matrices.projection = mat4.create();
    matrices.normal = mat3.create();
    matrices.viewMatrix = mat4.create();
}

function setupAttributes() {
    attributes.aVertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    attributes.aVertexColor = gl.getAttribLocation(shaderProgram, "aVertexColor");
    attributes.aVertexTextureCoord = gl.getAttribLocation(shaderProgram, "aVertexTextureCoord");
    attributes.uModelViewMatrix = gl.getUniformLocation(shaderProgram, "uModelViewMatrix");
    attributes.uProjectionMatrix = gl.getUniformLocation(shaderProgram, "uProjectionMatrix");
    attributes.uNormalMatrix = gl.getUniformLocation(shaderProgram, "uNormalMatrix");
    attributes.aVertexNormal = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    attributes.uLightPosition = gl.getUniformLocation(shaderProgram, "uLightPosition");
}

function setupBuffers() {
    buffers.vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.vertices), gl.STATIC_DRAW);

    buffers.colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.colors), gl.STATIC_DRAW);

    buffers.edgeBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.edgeBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cube.drawVertexIndices), gl.STATIC_DRAW);

    buffers.textureBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.textureBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.textureCoords), gl.STATIC_DRAW);

    buffers.verticesNormalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.verticesNormalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.normals), gl.STATIC_DRAW);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.colorBuffer);
    gl.vertexAttribPointer(attributes.aVertexColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attributes.aVertexColor);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.vertexBuffer);
    gl.vertexAttribPointer(attributes.aVertexPosition, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attributes.aVertexPosition);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.textureBuffer);
    gl.vertexAttribPointer(attributes.aVertexTextureCoord, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attributes.aVertexTextureCoord);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.edgeBuffer);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, cube.texture);
    gl.uniform1i(shaderProgram.uSampler2DId, 0);

    // light
    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.verticesNormalBuffer);
    gl.vertexAttribPointer(attributes.aVertexNormal, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attributes.aVertexNormal);
    mat3.normalFromMat4(matrices.normal, matrices.cube);
    gl.uniformMatrix3fv(attributes.uNormalMatrix, false, matrices.normal);
    var lightPositionEye = vec3.create();
    vec3.transformMat4(lightPositionEye, [0, 1.4, 1.1], matrices.viewMatrix);
    gl.uniform3fv(attributes.uLightPosition, lightPositionEye);

    mat4.perspective(matrices.projection, Math.PI / 4, canvas.width / canvas.height, -10, 1);
    mat4.lookAt(matrices.viewMatrix, [0, 2, 5], [0, 0, 0], [0, 1, 0]);
    mat4.rotateY(matrices.cube, matrices.viewMatrix, cube.rotation);
    mat4.rotateX(matrices.cube, matrices.cube, cube.rotation);
    mat4.rotateZ(matrices.cube, matrices.cube, cube.rotation);

    gl.uniformMatrix4fv(attributes.uProjectionMatrix, false, matrices.projection);
    gl.uniformMatrix4fv(attributes.uModelViewMatrix, false, matrices.cube);

    gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
}

function update() {
    cube.rotation = (cube.rotation + 0.01) % (2 * Math.PI);
}

function drawAnimation(timeStamp) {
    update();
    draw();
    window.requestAnimationFrame(drawAnimation);
}
